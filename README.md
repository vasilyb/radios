# DESCRIPTION

This project aims to create and support a set of really big playlists with
locators of online radio streams. Currently, the project contains only
playlist for electronic and dance online radio streams. It called
`electronic_dance.m3u`. This is a single playlist for electronic and dance
music and not two separate playlist because electronic music is also used to
dancing and dance music is often electronic.

There also is `all_formats.m3u` playlist. It will contain locators of
all the streams from all the playlists.
I am going to add more playlists shortly.

# LINKS TO SOURCES

[Listen music free](http://www.radiopotok.ru)

# USAGE

## Simpler solution

Just download the archive from the project
[main page](https://gitlab.com/vasilyb/radios) or using the next link:
[download](https://gitlab.com/vasilyb/radios/repository/archive.zip).
Then just unpack the archive.

```
unzip radios-master-455152f858d4ddcf92f029f0a7d7118c1d3a97b4.zip
```

Note that the commit number
(`455152f858d4ddcf92f029f0a7d7118c1d3a97b4` in the example)
will be different.

Then you will be able to load playlists from the directory where the archive
was unpacked to your media player such as
[DeaDBeeF](http://deadbeef.sourceforge.net/),
[MPlayer](http://mplayerhq.hu/design7/news.html),
[Videos](https://wiki.gnome.org/Apps/Videos) or
[xine](http://www.xine-project.org/home):bug:.

## Contributable solution

Just clone the repository.

```
git clone https://gitlab.com/vasilyb/radios.git
```

Use the next command each time when you wish get new version of playlists
from the online repository. Before executing the command you should `cd`
to the location of your clone of the repository.

```
git pull
```

# DEVELOPMENT TOOLS

You will need these tools only if you want contribute to the project.

Playlists (in context of the current project) are text files with `.m3u`
extension. Therefore you can use any editor from the next list.
any text editor you like to edit them. For example you can use
+ [Chet's Editor](http://www.texteditors.org/cgi-bin/wiki.pl?Chet%27s_Editor)
+ [zile](http://www.texteditors.org/cgi-bin/wiki.pl?Zile)
+ [MicroEmacs](http://www.texteditors.org/cgi-bin/wiki.pl?MicroEmacs)
+ [vi](http://www.texteditors.org/cgi-bin/wiki.pl?VI)
+ [Pluma](http://www.texteditors.org/cgi-bin/wiki.pl?Pluma)
+ [GEdit](http://www.texteditors.org/cgi-bin/wiki.pl?GEdit)

You must have `git` to easy get updates of playlist files easily or to
contribute your changes with others.

# CONTRIBUTION RULES

## Adding streams

If you wish to add a new stream to a playlist you should do several actions.

### Check if the steam already exist

Firstly, you must be sure that the stream is not already present in the
playlist. Search through the directory of your repository clone to fine
the stream location or radio station name. Do not duplicate streams.

### Add the stream

Secondly you should add a new entrie to playlist with according broadcasting
format. For example to `electronic_dance.m3u`.

```
#EXTINF:-1,ENERGY
http://ru2.101.ru:8000/v1_1?setst=
```

In the example:
+ `-1` is duration; do not leave `0`s in this field; it works but I like `-1`s
:wink:
+ `ENERGY` is the station name; please remove all extra data
(such as stream number or carrier frequency) from this field
+ `http://ru2.101.ru:8000/v1_1?setst=` is a stream locator; try to add URLs
of streams with the most high bitrate; do not add dead streams and check
added streams in your media player.

### Add the stream to `all_formats.m3u`

Copy added stream to the `all_formats.m3u` file.
Try to place it in the sequence of streams with according broadcasting
format. For example do not place dance stream among darkwave streams: it may
scare darkwawers:slight_smile:

And one more thing. Copy and paste the format to the name of the stream
copied to `all_formats.m3u` file. Look at the next example.

```
#EXTINF:-1,Electronic/Dance - ENERGY
http://ru2.101.ru:8000/v1_1?setst=
```

### Commit and push

Please, specify the name of the station you added or changed.
For example

```
git commit -a -m "add ENERGY"
git push -u origin master
```

Do not forget to `cd` to the directory yours clone of the repository
before executing the commands above.

## Removing or changing streams

### Do it!

When you found a dead stream, you should try to replace it with the working
one (of the same station, of course), because of the main reason streams
die is moving it to another locations. But if you sure the radio station is
closed, you may remove the stream from its playlist. _Do not forget to
update or drop the copy of the stream from `all_formats.m3u` file_.

### Share changes!

Commit the changes to your clone of the project repository.
Please, specify what stream has been updated or removed in the commit message.
Push changes to the online repository. Look at the next example.

```
git commit -a -m "change ENERGY to higher bit rate"
git push -u origin master
```

## Wiki

I am also going to create the wiki about listening to the radio using
different multimedia players. I think this is more convenient than using
a browser to do this. Also this desreases memory usage because media
players have less appetites than browser tabs with radio stream pages.
If you have time you may contribute to the wiki.

# Contacts

|                                 |                           |
|---------------------------------|---------------------------|
| Discussion conference in Jabber | ryazan@conference.xmpp.ru |
| Project master in Jabber        | vasily.blinkov@xmpp.ru    |

